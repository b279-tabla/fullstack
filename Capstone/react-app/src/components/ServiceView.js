import {Container, Card, Button, Row, Col, Modal, Form} from "react-bootstrap"
import { useParams } from "react-router-dom"
import {useState, useEffect, useContext} from "react"
import UserContext from "../UserContext"
import Swal from "sweetalert2"
import {Link} from "react-router-dom"
import { Navigate } from "react-router-dom";

export default function ServiceView(){

    const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	const { serviceId } = useParams();



    // useState for the retrieving Data from the MongoDB
	const [name, setName] = useState("");
	const [procedure, setProcedure] = useState("");
	const [price, setPrice] = useState(0);
	const [isAvailable, setIsAvailable] = useState(false);
	const [isActive, setIsActive] = useState(false);




    // useState for updating the Data from the MOongoDB in to new Data
    const [newName, setNewName] = useState("");
	const [newProcedure, setNewProcedure] = useState("");
	const [newPrice, setNewPrice] = useState(0);


	useEffect(() => {
		console.log(serviceId);

	}, [serviceId])
/* 		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`) */


    
	// Start for : retrieving data for Service Id
	function serviceData(e){
		e.preventDefault();
	}
		fetch (`http://localhost:5000/vet/${serviceId}`)
        

		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setProcedure(data.procedure);
			setPrice(data.price);
			setIsAvailable(data.isAvailable);
		})

	// End for : retrieving data for Service Id


	// Start for : Archiving (Set as not Available) using Service Id
	const archive = (serviceId) => {
		fetch(`http://localhost:5000/vet/${serviceId}/archive`, {
			method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isAvailable: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Archive Successful!",
                    icon: "success",
                    text: `TThe service is now Archieve`
                });

            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }
	// End for : Archiving (Set as not Available) using Service Id

	const unarchive = (serviceId) => {
		fetch(`http://localhost:5000/vet/${serviceId}/unarchive`, {
			method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isAvailable: true
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Unarchive Successful!",
                    icon: "success",
                    text: `The service is now Unarchieve`

                });

            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

	// Removing Service using the Service Id
	const deleteService = (serviceId) => {
		fetch(`http://localhost:5000/vet/${serviceId}/delete`, {
			method : "DELETE",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Unarchive Successful!",
                    icon: "success",
                    text: `The service is now Unarchieve`

                });

            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }
	
    /* EDIT PRODUCT */
    const editServices = (e) => {
        e.preventDefault();

        fetch(`http://localhost:5000/vet/${serviceId}/update`,{
            method: "PUT",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: newName,
                procedure: newProcedure,
                price: newPrice,
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "SERVICE EDIT SUCCESSFUL!!",
                    icon: "success",
                    text: "Service was edited successfully."
                });
                setNewName('');
                setNewProcedure('');
                setNewPrice(0);
            }
            else{

                Swal.fire({
                    title: "SERVICE EDIT UNSUCCESSFUL!",
                    icon: "error",
                    text: "The system is experiencing trouble at the moment. Please try again later."
                });

            }
        })


    }

	useEffect(() => {

		if (newName != "" && newProcedure != "" && newPrice > 0) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	
	}, [newName, newProcedure, newPrice]);


    return (


	<>
        <Row className="mt-5 pt-5">
			<Col lg={{ span: 6, offset: 3 }}>
			    <Card>
				{/* After retriving all Services Details, the Data (Name, Procedure, Price, will be place here) */}
					<Card.Body className="text-center" onSubmit={e => serviceData(e)}>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{procedure}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						<Card.Subtitle className="mb-3">You want to Edit Something:</Card.Subtitle>					

						{/* This is the button for the remove services as the method from the backend we will be using the service Id for the mapping of data we want to delete */}
						<Link className="btn btn-danger mx-2" to={`/AllServices`} onClick={() => deleteService(serviceId)}>Remove</Link>

						{/* This is the button for the Archiving services as the method from the backend we will be using the service Id for the mapping of data we want to Archive and Unarchive */}
						{
							// Statement for the Archiving button. 
							(isAvailable === true) ?
							// <Button className="mx-5" variant="primary" onClick={() => archive(serviceId)}>archive</Button>
							<Link className="btn btn-primary mx-2" to={`/AllServices`} onClick={() => archive(serviceId)}>Make it Unavailable</Link>
							:
							// <Button className="mx-5" variant="primary" onClick={() => unarchive(serviceId)}>Unarchive</Button>
							<Link className="btn btn-primary mx-2" to={`/AllServices`} onClick={() => unarchive(serviceId)}>Make it Available</Link>
						}
				    </Card.Body>		
				</Card>
	        </Col>
		</Row>

        <Row className="justify-content-center">
            <Col xs={12} md={3} xl={6} className="p-5 mx-1">

                <Form className='px-5' onSubmit={e => editServices(e)}>
                    
                    {/* Name */}
                    <Form.Group className="mb-3" controlId="FirstName">
                    <Form.Label>Procedure</Form.Label>
                    <Form.Control type="text" value={newName} onChange={e => setNewName(e.target.value)} placeholder="Enter First Name" required/>
                    </Form.Group>

                    {/* Procedure */}
                    <Form.Group className="mb-3" controlId="LastName">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" value={newProcedure} onChange={e => setNewProcedure(e.target.value)} placeholder="Enter Last Name" required/>
                    </Form.Group>

                    {/* Price */} 
                    <Form.Group className="mb-3" controlId="MobileNo">
                    <Form.Label>Price.</Form.Label>
                    <Form.Control type="text" value={newPrice} onChange={e => setNewPrice(e.target.value)} placeholder="Enter Mobile Number" required/>
                    </Form.Group>

                    {/* Submit Button */}

                    {isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">Submit Changes</Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>Submit Changes</Button>
                    }
                </Form>
            </Col>
        </Row>
	</>

    )
}
