import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar(){

  const { user } = useContext(UserContext);


    return (
        <Navbar className="NavigationBar px-5 py-2" expand="lg fixed-top">
            <Navbar.Brand 
                as={Link} to={"/"}
                className='px-5' size="lg" href="#home">
                <a className="NavLogo">Petto-ka</a>
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">

              <Nav className="ms-auto px-5">
        
              {
                (user.isAdmin ) ?
                <>

                <Nav.Link as={NavLink} to={"Dashboard"}
                className="mx-3">
                  <a className="NavLinks">Dashboard</a>
                </Nav.Link>
                <Nav.Link as={NavLink} to={"AllServices"}
                className="mx-3">
                  <a className="NavLinks">All Services</a>
                </Nav.Link>
                
                </>
                :
                <>
                <Nav.Link as={NavLink} to={"/"}
                className="mx-3">
                  <a className="NavLinks">Home</a>
                </Nav.Link>
                <Nav.Link as={NavLink} to={"Services"}
                className="mx-3">
                  <a className="NavLinks">Services</a>
                </Nav.Link>

                </>
              }




              {
                (user.token !== null) ?
                <>
                <Nav.Link as={NavLink} to={"Consultation"}
                className="mx-3">
                  <a className="NavLinks">Consultation</a>
                </Nav.Link>

                <Nav.Link as={NavLink} to={"Logout"}
                className="mx-3">
                  <a className="NavLinks">Logout</a>
                  </Nav.Link>
                
                </>
                :
                <>
                <Nav.Link as={NavLink} to={"Login"}
                className="mx-3">
                  <a className="NavLinks">Login</a>
                </Nav.Link>

                <Nav.Link as={NavLink} to={"Register"}
                className="mx-3">
                  <a className="NavLinks">Register</a>
                </Nav.Link>
                </>
              }
              </Nav>
            </Navbar.Collapse>
        </Navbar>
      );
}