import { Row, Col, Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom"
import UserContext from "../UserContext";
import { useContext } from "react";

export default function ServicesCard({serviceProp}){

    const { user } = useContext(UserContext);

    console.log(serviceProp.name)
    const { _id, name, procedure, price} = serviceProp;


    return(
        
        (user.isAdmin) ?

        <Row className="ServicesPage pt-5 justify-content-center">
            <Col xs={12} md={3} xl={5} className="mb-5 mx-1 ">
                <Card className="py-5 px-4">
                    <Card.Body>
                        <Card.Title className="text-start">
                            <h2 className="CardTitle">{name}</h2>
                        </Card.Title>
                        <Card.Text className="CardText text-start">
                            {procedure}
                        </Card.Text>
                        <Card.Text className="CardText text-start">
                            Price: {price}
                        </Card.Text>
                        <Link className="btn btn-primary" to={`/servicesView/${_id}`}>Details</Link>

                    </Card.Body>
                </Card>
            </Col>
        </Row> 

        :

        <Row className="ServicesPage pt-5 justify-content-center">
            <Col xs={12} md={3} xl={5} className="mb-5 mx-1 ">
                <Card className="py-5 px-4">
                    <Card.Body>
                        <Card.Title className="text-start">
                            <h2 className="CardTitle">{name}</h2>
                        </Card.Title>
                        <Card.Text className="CardText text-start">
                            {procedure}
                        </Card.Text>
                        <Card.Text className="CardText text-start">
                            Price: {price}
                        </Card.Text>
                        <Link className="btn btn-primary" to={`/serviceAppointment/${_id}`}>Make an Appointment!</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row> 
    )
}
