import LandingPage from "../components/LandingPage";

export default function Error(){

	const data = {
		title: "404 - NOT FOUND",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Home"
	}

	return(
		<LandingPage landingProps={data}/>
		)
}
