import {Form, Button} from "react-bootstrap";
import { Navigate, useNavigate, Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import { Row, Col} from "react-bootstrap";

export default function Register() {

    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() =>{


        if((firstName !== '' && lastName !=='' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !=='') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2])

    function registerUser(e){
        e.preventDefault();

        fetch(`http://localhost:5000/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "THE USER IS ALREADY REGISTERED",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            }
            else{

                fetch(`http://localhost:5000/users/register`,{
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                        
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "YOU ARE NOW REGISTERED!",
                            icon: "success",
                            text: "Please login to check your account."
                        });
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');
                        navigate("/login");
                    }
                    else{

                        Swal.fire({
                            title: "REGISTRATION NOT SUCCESSFUL",
                            icon: "error",
                            text: "Please try again later."
                        });

                    }
                })


            }
        })
    }

    return(
        (user.id !== null)
    ?
        <Navigate to="/" />
    :

    <Row>
        <Col xs={12} md={3} xl={6} className="p-5 mx-1">

            <Form className='px-5' onSubmit={e => registerUser(e)}>
                {/* First Name */}
                <Form.Group 
                    className="mb-3 mt-5" 
                    controlId="FirstName">

                <Form.Label>First Name</Form.Label>

                <Form.Control 
                    type="text" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    placeholder="Enter First Name" 
                    required
                    />
                </Form.Group>


                {/* Last Name */}
                <Form.Group 
                    className="mb-3" 
                    controlId="LastName">

                <Form.Label>Last Name</Form.Label>

                <Form.Control 
                    type="text" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    placeholder="Enter Last Name" 
                    required
                    />
                </Form.Group>


                {/* Mobile No. */}
                <Form.Group 
                    className="mb-3" 
                    controlId="MobileNo">

                <Form.Label>Mobile No.</Form.Label>
                <Form.Control 
                    type="text" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    placeholder="Enter Mobile Number" 
                    required
                    />
                </Form.Group>
                
                {/* Email */}
                <Form.Group 
                    className="mb-3" 
                    controlId="Email">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    placeholder="Enter email" 
                    required
                    />
                <Form.Text 
                    className="text-muted">We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>
            
                {/* Password */}
                <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password" 
                    placeholder="Enter Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                    type="password" 
                    placeholder="Verify Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required />
                </Form.Group>


                {/* Submit Button */}
                {
                isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }

            </Form>

        </Col>
    </Row>

    );
}
