import { Container,  Modal, Button, Col, Row, Form} from "react-bootstrap";
import ServicesCard from "../components/ServicesCard";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import {Link} from "react-router-dom";

export default function AllServices(){



    // User Identifier
    const { user } = useContext(UserContext);


    // userState of services data we get in mongodb
    // services is the getter, and setServices is the setter.
    const [services, setServices] = useState([]);




    // Model Function useState
    const [modalAdd, setModalAdd] = useState(false);
    const addServiceOpen = () =>setModalAdd(true);
    const addServiceClose = () => setModalAdd(false);



    // service Add function
    const [doctorName, setDoctorName] = useState('');
    const [procedure, setProcedure] = useState('');
    const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        

        /* For the User View All the Available Services will be shown in the interface including not Available. */


		fetch(`http://localhost:5000/vet/All`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setServices(data.map(services => {
				return (
					<ServicesCard key={services._id} serviceProp={services}/>
					)
			}))

		})
	}, [])

    const addService = (e) => {
    e.preventDefault();

    fetch(`http://localhost:5000/vet/`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            name: doctorName,
            procedure: procedure,
            price: price
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if (data) {
            Swal.fire({
                title: "PRODUCT ADDED SUCCESSFULLY!",
                icon: "success",
                text: `"The new product was added to the product list.`,
            });

            addServiceClose();
        }
        else {
            Swal.fire({
                title: "ADD PRODUCT UNSSUCCESSFUL!",
                icon: "error",
                text: `The system is experiencing trouble at the moment. Please try again later.`,
            });
            addServiceClose();
        }

    })
    setDoctorName('');
    setProcedure('');
    setPrice(0);
}


useEffect(() => {

    if (doctorName != "" && procedure != "" && price > 0) {
        setIsActive(true);
    } else {
        setIsActive(false);
    }

}, [doctorName, procedure, price]);


    return( 
        (user.isAdmin)
		?
        
        <>
        <Container fluid className="p-5 ServicesLandingPage text-center">
        <h1 className="ServicesH1 text-center mt-5 pt-5">What we Offer?</h1>
        <p className="ServicesP text-center mb-5">We offer a wide range of services which includes following below</p>

        {/* Start of Modal  */}
        <Button className="text-center" variant="primary" onClick={addServiceOpen}>
        ADD SERVICES
        </Button>
  
    <Modal show={modalAdd} onHide={addServiceClose}>
        <Form onSubmit={e => addService(e)}>
          <Modal.Header closeButton>
            <Modal.Title>ADD SERVICES</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Row>

                    {/* Doctor's Name Input */}
                    <Form.Group className="mb-3" controlId="doctorName">
                    <Form.Label>Doctor's name</Form.Label>
                    <Form.Control 
                    type="text" 
                    value={doctorName}
                    onChange={e => setDoctorName(e.target.value)}
                    placeholder="Enter First Name" 
                    required
                    />
                    </Form.Group>

                    {/* Procedure Input */}
                    <Form.Group className="mb-3" controlId="procedure">
                    <Form.Label>Procedure</Form.Label>
                    <Form.Control 
                    type="text" 
                    value={procedure}
                    onChange={e => setProcedure(e.target.value)}
                    placeholder="Enter Procedure" 
                    required
                    />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                    type="number" 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    placeholder="Enter Price Amount" 
                    required
                    />
                    </Form.Group>
            </Row>
          </Modal.Body>

            <Modal.Footer>

                        {/* // <Button className="mx-5" variant="primary" onClick={() => unarchive(serviceId)}>Unarchive</Button> */}

                                

                                

                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">ADD PRODUCT</Button>

                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>ADD PRODUCT</Button>

                            }
                            <Button variant="secondary" onClick={addServiceClose}>
                                Close
                            </Button>
            </Modal.Footer>


        </Form>
    </Modal>



        
        {services}
        </Container>
        </>
		:	

        <Navigate to="/dashboard"/>
    )
}