import courseData from "../data/courses";
import CourseCard from "../components/CourseCard";
import { useEffect, useState } from "react";


export default function Courses(){
    // Check to see if mock data was captured
    console.log(courseData);
    console.log(courseData[0]);

    const [allCourses, setAllCourses] = useState([])
    // Retrieves the course from database upon initial render of the "Courses Component"

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`, {
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setAllCourses(data.map(course => {
                return(
                    <CourseCard key={course._id} courseProp={course}/>
                )
            }))

        })
    },[])

    
 /*    const courses = courseData.map(course => {
        return(
            <CourseCard key={course.id} courseProp={course}/>
        );
    }) */

    return(
        <>
            <h1>Courses</h1>
            {/* Prop making and Prop passing */}
            {allCourses}
        </>
    )
};