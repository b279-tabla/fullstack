import Banner from "../components/Banner";

export default function Error() {

    const data = {
        title: "404: Not Found",
        content: "The page you are looking is not exist",
        destination: "/",
        label: "Home"
    }


    return(
        <Banner bannerProps = {data}/>
    );
}