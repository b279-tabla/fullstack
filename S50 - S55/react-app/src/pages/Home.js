import Banner from "../components/Banner";
import Highlights from "../components/Highlights";



export default function Home(){

    const data = {
        title: "Zuit Coding Bootcamp",
        content: "Oppoetunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll Now!"
    }


    return(
        <>
            <Banner bannerProps={data}/>
            <Highlights/>
        </>
    )
};