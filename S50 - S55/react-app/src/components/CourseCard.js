/* Activity S50 - S55 CourseCard */

import {  Card, Button } from "react-bootstrap";

/* 
	In React.js we have 3 hooks

	1. useState
	2. useEffect
	3. use Context

*/

import {useState} from "react";
import { Link } from "react-router-dom";


export default function CourseCard({courseProp}){

	// Check if props was successfully passed
	console.log(courseProp.name);
	console.log(courseProp.description);
	console.log(courseProp.price);
	// checks the type of data
	console.log(typeof courseProp);

	// Destrring the courseProp into their own variables
	const { _id, name, description, price } = courseProp;

/* 	// useState
	// Used for storing different states
	// used to keep track of information to individual components

	// SYNTAX -> const [getter, setter] = useState(intitialGetterValue)

	const [count, setCount] = useState(0);
	console.log(useState(0));

	// Activity S51 - Limits seat and enrolled by 30 enrollees
	const [seat, setSeat] = useState(30);


	// Function that keeps track of the enrollees for a course
	function enrollees(){

		if(count >= 30 && seat <= 0 ){

			return alert("No more seats");
		}else{
			
			setCount(count + 1);
			setSeat(seat - 1);
		}

		console.log("Enrolless: " + count);
		console.log("Available Seats: " + seat);
	} */

	return(
		<Card className="cardCourseCard my-3">
			<Card.Body>
				{/* Title Card */}
				<Card.Title>{name}</Card.Title>

				{/* Description Card */}
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				{/* Price Card */}
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>

				<Link 
				className="btn btn-primary" 
				to={`/courseView/${_id}`}>
					Details
				</Link>



{/* 				<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count}</Card.Text> */}

{/* 				<Card.Text>{seat}</Card.Text>
				<Button className="btn-primary" onClick={enrollees}>Enroll</Button> */}
			</Card.Body>
		</Card>
	);
};


