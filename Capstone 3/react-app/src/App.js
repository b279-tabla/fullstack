/* import './App.css'; */
import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Services from './pages/Services';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error";
import Dashboard from './pages/Dashboard';

import { Container } from "react-bootstrap";
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter as Router} from "react-router-dom";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";



function App() {

 // Creating a user state for global scope

  /*const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })*/

  const [user, setUser] = useState({
    id: localStorage.getItem("id"),
    isAdmin: localStorage.getItem("isAdmin"),
    email: localStorage.getItem("email"),
    token: localStorage.getItem("token")
})

// Function for clearing the storage on logout
const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {
  console.log(user);
  console.log(localStorage);
}, [user])




  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
        <Container fluid>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path='/Services' element={<Services/>}/>
            <Route path='/Register' element={<Register/>}/>
            <Route path='/Login' element={<Login/>}/>
            <Route path='/Logout' element={<Logout/>}/>
            <Route path="*" element={<Error/>}/>
            <Route path="/dashboard" element={<Dashboard />} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>
  )
};

export default App;
