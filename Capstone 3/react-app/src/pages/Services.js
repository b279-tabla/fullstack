import { Container } from "react-bootstrap";
import ServicesCard from "../components/ServicesCard";
import serviceData from "../data/services";
import { useEffect, useState } from "react";

export default function Services(){

    

    const services = serviceData.map(service => {
        return(
            <ServicesCard key={service.id} serviceProp={service}/>
        )
    })

    return(
        <Container fluid className="p-5 ServicesLandingPage">
        <h1 className="ServicesH1 text-center mt-5 pt-5">What we Offer?</h1>
        <p className="ServicesP text-center mb-5">We offer a wide range of services which includes following below</p>
        {services}
        </Container>
    )
}