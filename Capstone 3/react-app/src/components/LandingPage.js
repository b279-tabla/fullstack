import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function LandingPage({landingProps}){

    console.log(landingProps);

    const {title, content, destination, label} = landingProps;


	return(
        <Row className="LandingPage p-5 mt-5 mh-100">
            <Col xs={12} md={12} xl={6} className="">
                <p className="VetinaryClinic ps-5">Vetinary Clinic</p>
                <h1 className="LandingPageH1 ps-5 mt-4">{title}</h1>
                <p className="LandingPageP ps-5 mt-4">{content}</p>
                <Button as={Link} to={destination} className="LandingPageButton btn-primary ms-5 my-5">
                <h2 className="LandingPageButtont">{label}</h2>
                </Button>
            </Col>
        </Row>
    );
}
