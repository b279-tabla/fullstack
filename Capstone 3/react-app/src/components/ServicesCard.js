import { Row, Col, Card} from "react-bootstrap";

export default function ServicesCard({serviceProp}){

    console.log(serviceProp.name)
    const { name, description, price} = serviceProp;

    return(

        <Row className="ServicesPage pt-5 justify-content-center">
            <Col xs={12} md={3} xl={5} className="mb-5 mx-1 ">
                <Card className="py-5 px-4">
                    <Card.Body>
                        <Card.Title className="text-start">
                            <h2 className="CardTitle">{name}</h2>
                        </Card.Title>
                        <Card.Text className="CardText text-start">
                            {description}
                        </Card.Text>
                        <Card.Text className="CardText text-start">
                            Price: {price}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row> 
    )
}
